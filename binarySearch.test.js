// const binarySearch=require('./binarySearch')
import binarySearch from "./binarySearch.js"

test ('empty array is passed and gets an error', () =>{
    expect(binarySearch([],8)).toBe(-1)
})
test ("get correct result if we insert first element as search" , () => {
    expect(binarySearch([0,1,2,3],0)).toBe(0)
})
test ("get correct result if we insert last element as search" , () => {
    expect(binarySearch([0,2,3,4,6,8],8)).toBe(5)
})
