const divide = require("./script");

test ( 'divide 4 by 2 equal to 2' , () => {
    expect (divide(4,2)).toBe(2);
});
test ( "if b is 0 it throws an error" , () => {
    expect (divide (4,0)).toBe ("error")
})
test ("if a is not a number b is a number o/p error" , () => {
    expect (divide ("hii",0)).toBe("error")
})
test ( "if a is a number b is not a number o/p error" , ()=> {
    expect (divide(2,"kitty")).toBe("error")
})
test ( "if a is not a number b is not a number o/p error" , ()=> {
    expect (divide("puppy","kitty")).toBe("error")
})